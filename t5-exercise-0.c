#include <stdio.h>
#include <stdlib.h>

int main() {
    int *array;
    int n, i, sum = 0;

    printf("Please input the size of array (1 to 20):");
    scanf("%d", &n);
    if (n < 1 || n > 20) {
        printf("The number given is out of range.");
        return 1;
    }

    array = (int *) malloc(n * sizeof(int));

    for (i = 0; i < n; i++) {
        sum += (i + 1);
        *(array + i) = sum;
    }

    printf("The cumulative sum of 1 to %d is (reverse order):\n", n);
    for (i = n - 1; i >= 0; i--) {
        printf("%d ", array[i]);
    }

    free(array);
    return 0;
}
