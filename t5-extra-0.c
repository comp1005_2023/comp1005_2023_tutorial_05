#include <stdio.h>
#include <stdlib.h>

int main() {
    int **array;
    int m, n, i, j, k, sum;

    printf("Please input two numbers as the size of the matrix (maximum 10 x 10): ");
    scanf("%d %d", &m, &n);
    if (m < 1 || m > 10 || n < 1 || n > 10) {
        printf("The number(s) given is(are) out of range.");
        return 1;
    }

    printf("The matrix of (m=%d, n=%d) is:\n", m, n);

    array = (int **) malloc(m * sizeof(int *));
    for (i = 0, k = 1, sum = 0; i < m; i++) {
        array[i] = (int *) malloc(n * sizeof(int));
        for (j = 0; j < n; j++) {
            array[i][j] = sum += k++;
            printf("%5d", array[i][j]);
        }
        printf("\n");
    }

    for (i = 0; i < m; i++) {
        free(array[i]);
    }
    free(array);
    return 0;
}
