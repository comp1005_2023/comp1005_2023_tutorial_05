COMP1005 2023 Tutorial 5
========================


## Background

* [Dynamic allocation: Part A](https://www.learn-c.org/en/Dynamic_allocation)
* [Dynamic allocation: Part B](https://www.tutorialspoint.com/cprogramming/c_memory_management.htm)

## Exercises

0. Write a program in C to create an array of size n with dynamic memory allocation to store the cumulative sum from 1 to n. Print the cumulative in reverse order.  
    ***Expected Output:*** 
    ```
    Please input the size (n) of array (n <= 20): 5
    
    The cumulative sum of 1 to 5 is (reverse order): 
    15 10 6 3 1
    ```

0. Go back to tutorial 3, try those exercies (including the extras) again with dynamic memory allocation where applicable.

## Extras

0. Write a program in C to create a 2D array of size (m, n) with dynamic memory allocation to store the cumulative sum from 1 to m*n. Print the array in the matrix format (by row).  
    ***Expected Output:*** 
    ```
    Input the row number m: 2
    Input the column number n: 3
    
    The desired matrix (m=2, n=3) is:
     1  3  6
    10 15 21
    ```

